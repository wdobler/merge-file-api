*Merge-File-API*: A Python interface to file merging and merge conflicts.

[[_TOC_]]

# Overview

*Merge-File-API* provides Python access to the different parts of a file
that get marked up when there are merge conflicts.

For example, a merge conflict that Git would represent in the form

```
    Common first
    and second lines
    <<<<<<< HEAD
    Third line on our branch
    ||||||| 4ba6d744193
    A third line nobody wanted to keep
    =======
    Third and fourth line
    on their branch
    >>>>>>> 2d5149fe29c
    All that remains
    is pure
    harmony.
```

will be represented as a list of `MergePart` objects:

```python
    [
        Consensus(text=["Common first", "and second lines"]),
        Conflict(
            ours=["Third line on our branch"],
            base=["A third line nobody wanted to keep"],
            theirs=["Third and fourth line", "on their branch"]
        )
        Consensus(text=["All that remains", "is pure", "harmony"])
    ]
```

This is in particular useful for writing custom merge drivers.


# Scripts

There are two scripts in the `scripts/` directory that illustrate how a
commit is represented:

```sh
    annotate-merge.py file-with-conflict-markers
    merge-file.py base-file ours-file theirs-file
```

Both scripts prefix each line with `:` (consensus), `<` (ours), `|`
(base), `>` (theirs).
While `annotate-merge` parses an existing file with conflict markers,
`merge-file` takes the three variants of a file and calls `git merge-file`
to identify each line's merge state.


# Implementation

For now, the implementation is quite simple.
We call `git merge-file --diff3 --stdout` with custom markers and parse
the output.


# Requirements

*Merge-File-API* currently requires

- Python 3.6.9 or later,
- Git
