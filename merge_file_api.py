#!/usr/bin/python3
# -*- coding: utf-8 -*-   vim: set fileencoding=utf-8 :

"""Data structures and methods for merging two text files.

This module represents the result from git-merge-file as a list of MergePart
objects, where each MergePart is either a Consensus or a Conflict.

"""


from enum import Enum, auto
import re
import subprocess
from typing import List, Optional, Pattern


class Text:
    """Convenience class for representing lines of text."""

    def __init__(self, lines: List[str]):
        self.lines = lines

    def __repr__(self) -> str:
        return "\n".join(self.lines)

    def __iter__(self):
        return iter(self.lines)

    def __bool__(self):
        return bool(self.lines)


class MergePart:
    """Abstract base class for Consensus and Conflict."""

    pass


class Consensus(MergePart):
    """Both parties agree on the text."""

    def __init__(self, text: Text):
        self.text = text

    def __repr__(self) -> str:
        return "Consensus[{}]".format(self.text)


class Conflict(MergePart):
    """Two versions of the text."""

    def __init__(self, ours: Text, base: Text, theirs: Text) -> None:
        self.ours = ours
        self.base = base
        self.theirs = theirs

    def __repr__(self) -> str:
        return "Conflict[{},\n{},\n{}]".format(
            self.ours, self.base, self.theirs
        )


class DiffParser:
    """Parse the output from git-merge-file."""

    MARKER_SIZE = 113
    OURS_TEXT = "TEXTMERGE_OURS"
    BASE_TEXT = "TEXTMERGE_BASE"
    THEIRS_TEXT = "TEXTMERGE_THEIRS"

    class _State(Enum):
        CONSENSUS = auto()
        OURS = auto()
        BASE = auto()
        THEIRS = auto()

        @staticmethod
        def transition_allowed(
            old: "DiffParser._State", new: "DiffParser._State"
        ) -> bool:
            """Only some state transitions are allowed.

            These are:
            1. Stay in same state
            2. The sequence CONSENSUS → OURS → BASE → THEIRS → CONSENSUS

            """

            allowed = {}
            for state in DiffParser._State:
                allowed[(state, state)] = True
            allowed[
                (DiffParser._State.CONSENSUS, DiffParser._State.OURS)
            ] = True
            allowed[
                (DiffParser._State.OURS, DiffParser._State.BASE)
            ] = True
            allowed[
                (DiffParser._State.BASE, DiffParser._State.THEIRS)
            ] = True
            allowed[
                (DiffParser._State.THEIRS, DiffParser._State.CONSENSUS)
            ] = True
            return (old, new) in allowed

    class _Block:
        """A block of text, with a parser state"""

        def __init__(
            self, state: "DiffParser._State", text: Text
        ) -> None:
            self.state = state
            self.text = text

        def __str__(self) -> str:
            return "{}: {}".format(self.state, self.text)

    def __init__(self) -> None:
        self._blocks = []  # type: List[DiffParser._Block]
        self._lines = []  # type: List[str]
        self.state = DiffParser._State.CONSENSUS  # to start with

    def feed(self, line: str) -> None:
        new_state = self._state_from_line(self.state, line)
        if new_state == self.state:
            self._lines.append(line)
        else:
            self._blocks.append(
                DiffParser._Block(self.state, Text(self._lines))
            )
            self._lines = []
            self.state = new_state

    def finish(self) -> List[MergePart]:
        self._blocks.append(
            DiffParser._Block(self.state, Text(self._lines))
        )
        parts = []  # type: List[MergePart]
        pending_ours = None  # type: Optional[Text]
        pending_base = None  # type: Optional[Text]
        for block in self._blocks:
            if block.state is DiffParser._State.CONSENSUS:
                parts.append(Consensus(block.text))
            elif block.state is DiffParser._State.OURS:
                assert (
                    pending_ours is None
                ), "'Ours' block {} not consumed".format(pending_ours)
                pending_ours = block.text
            elif block.state is DiffParser._State.BASE:
                assert pending_ours is not None, "Missing 'ours' block"
                assert (
                    pending_base is None
                ), "'Base' block {} not consumed".format(pending_base)
                pending_base = block.text
            elif block.state is DiffParser._State.THEIRS:
                assert pending_ours is not None, "Missing 'ours' block"
                assert pending_base is not None, "Missing 'base' block"
                parts.append(
                    Conflict(
                        ours=pending_ours,
                        base=pending_base,
                        theirs=block.text,
                    )
                )
                pending_ours = None
                pending_base = None

            else:
                raise Exception("Bad block state for ", block)
        return parts

    def _state_from_line(
        self, prev_state: "DiffParser._State", line: str
    ) -> "DiffParser._State":
        start_ours = self._marker_pattern(r"<", self.OURS_TEXT)
        start_base = self._marker_pattern(r"\|", self.BASE_TEXT)
        separator = self._marker_pattern(r"=")
        end_theirs = self._marker_pattern(r">", self.THEIRS_TEXT)
        if re.search(start_ours, line):
            state = DiffParser._State.OURS
        elif re.search(start_base, line):
            state = DiffParser._State.BASE
        elif re.search(separator, line):
            state = DiffParser._State.THEIRS
        elif re.search(end_theirs, line):
            state = DiffParser._State.CONSENSUS
        else:
            state = prev_state
        if DiffParser._State.transition_allowed(prev_state, state):
            return state
        else:
            raise Exception(
                "Transition {} → {} is not allowed".format(
                    prev_state, state
                )
            )

    def _marker_pattern(
        self, char: str, label: Optional[str] = None
    ) -> Pattern[str]:
        pattern = "%s{%s}" % (char, DiffParser.MARKER_SIZE)
        if label is not None:
            pattern += " " + label
        return re.compile("^" + pattern + "$")


def merge_files(ancestor: str, ours: str, theirs: str) -> List[MergePart]:
    """Run git-merge-file and represent the result as MergePart's."""
    cmd_line = [
        "git",
        "merge-file",
        "--diff3",
        "--stdout",
        ours,
        ancestor,
        theirs,
        "--marker-size",
        str(DiffParser.MARKER_SIZE),
        "-L",
        DiffParser.OURS_TEXT,
        "-L",
        DiffParser.BASE_TEXT,
        "-L",
        DiffParser.THEIRS_TEXT,
    ]
    parser = DiffParser()
    p = subprocess.Popen(
        cmd_line, stdout=subprocess.PIPE, universal_newlines=True
    )
    assert p.stdout is not None  # appease mypy
    for line in p.stdout:
        parser.feed(line.rstrip())
    return parser.finish()


def read_merged_file(merge_file: str) -> List[MergePart]:
    """Read and parse a file with standard conflict markers.

    This works for conflict markers of the standard form produced by 'git
    merge-file --diff3'.

    """
    parser = DiffParser()

    def convert_to_diff_parser_format(
        line: str,
        marker1: str,
        marker2: str,
        diff_parser_text: Optional[str],
    ) -> str:
        """Convert standard conflict markers to DiffParser variants."""
        pattern = r"^" + marker1 + r"{7}" + ".*"
        diff_parser_marker = marker2 * DiffParser.MARKER_SIZE
        if diff_parser_text is not None:
            diff_parser_marker += " " + diff_parser_text
        return re.sub(pattern, diff_parser_marker, line)

    with open(merge_file) as fh:
        for line in fh:
            line1 = line.rstrip()
            for (marker1, marker2, text) in [
                (r"<", "<", DiffParser.OURS_TEXT),
                (r"\|", "|", DiffParser.BASE_TEXT),
                (r"=", "=", None),
                (r">", ">", DiffParser.THEIRS_TEXT),
            ]:
                line1 = convert_to_diff_parser_format(
                    line1, marker1, marker2, text
                )
            parser.feed(line1)
    return parser.finish()
