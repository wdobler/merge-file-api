#!/usr/bin/python3
# -*- coding: utf-8 -*-   vim: set fileencoding=utf-8 :

__doc__ = """Print the contents of a merged file.

Prefix each line with ': ' (consensus), '< ' (ours), '| ' (base), or '> '
(theirs).

"""


import argparse

from merge_file_api import read_merged_file, Consensus, Conflict, Text


def main() -> None:
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Indicate empty blocks using the <EMPTY> marker.",
    )
    parser.add_argument(
        "file", help="The file to parse (normally contains merge markers)"
    )

    args = parser.parse_args()
    verbose = args.verbose

    merge = read_merged_file(args.file)
    for part in merge:
        if isinstance(part, Consensus):
            print_prefixed(": ", part.text, verbose)
        elif isinstance(part, Conflict):
            print_prefixed("< ", part.ours, verbose)
            print_prefixed("| ", part.base, verbose)
            print_prefixed("> ", part.theirs, verbose)


def print_prefixed(pref: str, text: Text, verbose: bool) -> None:
    if text:
        print("\n".join([pref + line for line in text]))
    else:
        if verbose:
            print(pref + "<EMPTY>")


if __name__ == "__main__":
    main()
