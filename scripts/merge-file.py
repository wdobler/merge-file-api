#!/usr/bin/python3
# -*- coding: utf-8 -*-   vim: set fileencoding=utf-8 :

__doc__ = """Print the results of merging a file using 'git merge-file'.

Read three files, representing the ancestor/ours/theirs states of the merge.
Print the merged lines, pefixing each with ': ' (consensus), '< ' (ours),
'| ' (base), or '> ' (theirs).

"""


import argparse

from merge_file_api import merge_files, Consensus, Conflict, Text


def main() -> None:
    parser = argparse.ArgumentParser(
        description=__doc__,
        epilog="Note that the order of the arguments differs from"
        " git-merge-file, so "
        " 'merge-file.py base, ours, theirs' calls"
        " 'git merge-file --diff3 --stdout ours, base, theirs'.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Indicate empty blocks using the <EMPTY> marker.",
    )
    parser.add_argument(
        "base-file",
        help="Name of the file representing the merge-base of ours and theirs",
    )
    parser.add_argument(
        "ours-file",
        help="Name of the file representing the content into which to merge.",
    )
    parser.add_argument(
        "theirs-file",
        help="Name of the file representing the content to merge in.",
    )

    args = parser.parse_args()
    verbose = args.verbose

    merge = merge_files(
        getattr(args, "base-file"),
        getattr(args, "ours-file"),
        getattr(args, "theirs-file"),
    )
    for part in merge:
        if isinstance(part, Consensus):
            print_prefixed(": ", part.text, verbose)
        elif isinstance(part, Conflict):
            print_prefixed("< ", part.ours, verbose)
            print_prefixed("| ", part.base, verbose)
            print_prefixed("> ", part.theirs, verbose)


def print_prefixed(pref: str, text: Text, verbose: bool) -> None:
    if text:
        print("\n".join([pref + line for line in text]))
    else:
        if verbose:
            print(pref + "<EMPTY>")


if __name__ == "__main__":
    main()
