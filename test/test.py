#!/usr/bin/python3
# -*- coding: utf-8 -*-   vim: set fileencoding=utf-8 :


from textmerge import merge_files, Consensus, Conflict


def main():
    merge = merge_files(ancestor="ancestor", ours="ours", theirs="theirs")

    print("--------------------")
    print("Union merge:")
    print("--------------------")
    print_union(merge)

    print("--------------------")
    print("Reverse union merge:")
    print("--------------------")
    print_reverse_union(merge)

    print("--------------------")
    print("Reverse union merge:")
    print("--------------------")
    print_labelled(merge)

def print_union(merge):
    """Print the equivalent of 'git-merge-file --union'.

    I.e. the output from 'git-merge-file' with all marker lines removed.

    """
    for part in merge:
        if isinstance(part, Consensus):
            print(str(part.text))
        elif isinstance(part, Conflict):
            print(str(part.ours))
            print(str(part.theirs))


def print_reverse_union(merge):
    """Print the complement of 'git-merge-file --union'.

    I.e. for conflicts, print 'theirs' first, then 'ours'.

    """
    for part in merge:
        if isinstance(part, Consensus):
            print(str(part.text))
        elif isinstance(part, Conflict):
            print(str(part.theirs))
            print(str(part.ours))

def print_labelled(merge):
    """Print the whole merge, prefixing each line with a label."""

    for part in merge:
        if isinstance(part, Consensus):
            print(prefix("Consensus ", part.text))
        elif isinstance(part, Conflict):
            print(prefix("Ours      ", part.ours))
            print(prefix("Theirs    ", part.theirs))
            print(prefix("Base      ", part.base))


def prefix(pref, text):
    if text.lines:
        return "\n".join([pref + ":" + line for line in text.lines])
    else:
        return pref + ":" + "<EMPTY>"


if __name__ == "__main__":
    main()
